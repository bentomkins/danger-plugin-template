# danger-plugin-template

[![Build Status](https://travis-ci.org/45085606/danger-plugin-template.svg?branch=master)](https://travis-ci.org/45085606/danger-plugin-template)
[![npm version](https://d25lcipzij17d.cloudfront.net/badge.svg?id=js&r=r&type=6e&v=deadbeef&x2=0)](https://badge.fury.io/js/danger-plugin-template)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

> default sample template

## Usage

Install:

```sh
yarn add danger-plugin-template --dev
```

At a glance:

```js
// dangerfile.js
import template from 'danger-plugin-template'

template()
```
## Changelog

See the GitHub [release history](https://github.com/45085606/danger-plugin-template/releases).

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).
